---
layout: job_family_page
title: "UX Management"
---


## UX Management Roles at GitLab

Managers in the UX department at GitLab see the team as their product. While they are credible as designers and know the details of what Product Designers work on, their time is spent hiring a world-class team and putting them in the best position to succeed. They own the delivery of UX commitments and are always looking to improve productivity. They must also coordinate across departments to accomplish collaborative goals.

### Product Design Manager

The Product Design Manager reports to the Director of Product Design, and Product Designers report to the Product Design Manager.

#### Responsibilities

* Identify improvements for UX design (e.g. look and feel, color, spacing, etc.).
* Hire a world class team of Product Designers.
* Help Product Designers grow their skills and expertise.
* Hold regular 1:1s with all members of their team.
* Create a sense of psychological safety on their team.
* Recommend UX technical and process improvements.
* Give clear, timely, and actionable feedback.
* Improve scheduling process to balance necessary UX improvements.
* Work with all parts of the organization (e.g. Backend, Frontend, Build, etc.) to improve overall UX.
* Strong sense of ownership, urgency, and drive.
* Excellent written and verbal communication skills, especially experience with executive-level communications.
* Ability to make concrete progress in the face of ambiguity and imperfect knowledge.

#### Requirements

* A minimum of 3 years experience managing a group of designers.
* Solid visual awareness with understanding of basic design principles like typography, layout, composition, and color theory.
* Proficiency with pre-visualization software (e.g. Sketch, Adobe Photoshop, Illustrator).
* Experience defining the high-level strategy (the why) and creating design deliverables (the how) based on research.
* Experience driving organizational change with cross-functional stakeholders.
* Passion for creating visually pleasing and intuitive user experiences.
* Collaborative team spirit with great communication skills.
* You share our [values](/handbook/values), and work in accordance with those values.
* [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#management-group).
* Ability to use GitLab.

**NOTE** In the compensation calculator below, fill in "Manager" in the `Level` field for this role.


#### Interview Process

* [Screening call](/handbook/hiring/#screening-call) with a recruiter.
* Interview with Product Designer. In this interview, the interviewer will spend a lot of time trying to understand the experience you have as a manager, as well as what type of teams you have led and your management style. The interviewer will also be looking to understand how you define strategy, how you work with researchers, how you've handled conflict, and how you dealt with difficult situations in the past. Do be prepared to talk about your work, experience with Design Systems, and technical ability, too. 
* Interview with Product Design Manager. In this interview, we want you to share a case study presentation that provides insight to a problem you solved as part of a project you led. We'll look to understand the size and structure of your team, the goals of the project, the low-fidelity design work, the high-fidelity design output, how you/the team approached research, how you synthesized research data to inform design decisions, what design standards and guidelines you worked within, and how you collaborated with the wider team. Broadly, we want to hear how you identified what needed to be done and then guided your team to the end result.
* Interview with UX Director. In this interview, the interviewer will spend a lot of time trying to understand the experience you have as a manager, what types of teams you have led, and your management style. The interviewer will also want to understand how you define strategy, how you work with researchers, how you've handled conflict, and how you've dealt with difficult situations in the past. Do be prepared to talk about your work, experience with Design Systems, and technical ability, too.
* Interview with Product Director.
* Interview with VP of Engineering.

### Senior Product Design Manager

The Senior Product Design Manager reports to the Director of Product Design, and the Product Design Manager reports to the Senior Product Design Manager.

#### Responsibilities

* Define and manage performance indicators for the Product Design team by actively contributing to the product design KPIs on the [UX KPIs](/handbook/engineering/ux/performance-indicators/) page in the handbook.
* Actively advocate for Product Design throughout the organization by ensuring Product Design responsibilities are reflected in the Product Development Flow.
* Help drive cross-product workflows by having an awareness of what's happening across all sections through active participation in design reviews, UX Showcases, and Group Conversations.
* Faciliatate the creation and execution of product design [OKRs](https://about.gitlab.com/company/okrs/) in collabration with the Product Design team and UX Leadership.
* Ensure UX is prioritized by working with product leadership to identify opportunities for validation and better cross-functional collaboration.
* Coach Product Design Managers on how to conduct 1:1s, growth, and feedback conversations with their direct reports.
* Conduct quarterly skip levels with your reports' direct reports.
* Participate in Opportunity Canvas reviews by asking questions and providing feedback that focus on a great user experience.
* Hire a world-class team of Product Designers and Product Design Managers.

#### Requirements

* A minimum of 3 years experience managing Product Design Managers.
* Solid visual awareness with understanding of basic design principles like typography, layout, composition, and color theory.
* Experience defining the high-level strategy (the why) and helping your team tie design and research back to that strategy.
* Experience driving organizational change with cross-functional stakeholders.
* Passion for creating visually pleasing and intuitive user experiences.
* Collaborative team spirit with great communication skills.
* You share our [values](/handbook/values), and work in accordance with those values.
* [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#management-group).
* Ability to use GitLab.

**NOTE** In the compensation calculator below, fill in "Manager" in the `Level` field for this role.


#### Interview Process

* [Screening call](/handbook/hiring/#screening-call) with a recruiter.
* Interview with Product Design Manager. In this interview, the interviewer will focus on understanding your experience with driving design strategy, managing managers, and influencing the wider organization in which you worked. Examples of large, complex projects that had a significant impact to product experience will be helpful. Broadly, we want to hear how you identified what needed to be done and then guided your team to the end result.
* Interview with UX Director. In this interview, the interviewer will spend a lot of time trying to understand the experience you have as a manager, what types of teams you have led, and your management style. The interviewer will also want to understand how you define strategy, how you work with researchers, how you've handled conflict, and how you've dealt with difficult situations in the past. Do be prepared to talk about your work, experience with Design Systems, and technical ability, too.
* Interview with Product Director.
* Interview with VP of Engineering.

### Director of Product Design

The Director of Product Design reports to the UX Director, and Product Design Managers and Senior Product Design Managers report to the Director of Product Design.

The Director of Product Design role extends the Senior Product Design Manager role.

#### Responsibilities

* Define and manage performance indicators for the Product Design team by independently managing product design KPIs on the [UX KPIs](/handbook/engineering/ux/performance-indicators/) page in the handbook.
* Actively advocate for Product Design throughout the organization by ensuring Product Design responsibilities are reflected in the Product Development Flow.
* Help drive cross-product workflows by having an awareness of what's happening across all sections through active participation in design reviews, UX Showcases, and Group Conversations.
* Independently manage the creation and execution of product design [OKRs](https://about.gitlab.com/company/okrs/) with feedback from the Product Design team and UX Leadership.
* Ensure UX is prioritized by working with product leadership to identify opportunities for validation and better cross-functional collaboration.
* Communicate significant product design strategy decisions to leadership and the wider company.
* Coach Product Design Managers on how to conduct 1:1s, growth, and feedback conversations with their direct reports.
* Conduct quarterly skip levels with your reports' direct reports.
* Participate in Opportunity Canvas reviews by asking questions and providing feedback that focus on a great user experience.
* Hire a world-class team of Product Designers and Product Design Managers.

#### Requirements

* A minimum of 10 years experience managing designers, and leading design for a product company.
* Solid visual awareness with understanding of basic design principles like typography, layout, composition, and color theory.
* Proficiency with pre-visualization software (e.g. Sketch, Adobe Photoshop, Illustrator).
* Experience defining the high-level strategy (the why) and creating design deliverables (the how) based on research.
* Passion for creating visually pleasing and intuitive user experiences.
* Collaborative team spirit with great communication skills.
* You share our [values](/handbook/values), and work in accordance with those values.
- [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#director-group)

#### Interview Process

* [Screening call](/handbook/hiring/#screening-call) with a recruiter.
* Interview with Product Designer. In this interview, the interviewer will spend a lot of time trying to understand the experience you have as a manager, as well as what type of teams you have led and your management style. The interviewer will also be looking to understand how you define strategy, how you work with researchers, how you've handled conflict, and how you dealt with difficult situations in the past. Do be prepared to talk about your work, experience with Design Systems, and technical ability, too.
* Interview with UX Manager (peer). In this interview, the interviewer will spend a lot of time trying to understand the experience you have as a manager, as well as what type of teams you have led and your management style. The interviewer will also be looking to understand how you define strategy, how you work with researchers, how you've handled conflict, and how you dealt with difficult situations in the past. Do be prepared to talk about your work, experience with Design Systems, and technical ability, too.
* Interview with UX Director. In this interview, we will be looking for you to give some real insight into a problem you were solving as part of project you've led the work on. We'll look to understand the size and structure of the team you were a part of, the goals of the project, the low-fidelity design work, the high-fidelity design output, how you/the team approached research, how you synthesised research data to inform design decisions, what design standards and guidelines you worked within, and how you collaborated with the wider team. Broadly, we want to hear how you identified what needed to be done and then guided your team to the end result.
* Interview with Product Director.
* Interview with VP of Engineering.

### Vice President of User Experience

The Vice President of User Experience reports to the Executive Vice President of Engineering, and Senior Managers and Directors of Product Design, UX Research, and Technical Writing report to the Vice President of User Experience.

#### Responsibilities

* Align Product Design, UX Research, and Technical Writing to company objectives, and communicate broadly about important UX initiatives, setting an ambitious UX vision for the department, product, and company.
* Manage the UX budget, including compensation planning, non-headcount budget allocation, and tradeoff decisions.
* Interface regularly with executives on important decisions.
* Identify ways to elevate the GitLab product experience, manage initiatives to address those concerns, and track and communicate about progress.
* Help UX leaders grow their skills and leadership experience.
* Foster an open and collaborative culture based on trust in the UX department, where everyone feels empowered to do their best work.
* Ensure that UX is well-integrated into the [Product Development Flow](/handbook/product-development-flow/), and advocate for process changes that help product management, engineering, and UX work together to build a great experience.
* Establish and promote design guidelines, best practices, and standards, and help to drive GitLab's [design system](https://design.gitlab.com/) forward at a strategic level.
* Work with product leadership to prioritize research efforts, so that we validate whether we're solving the right problems in the right ways.
* Define value-driven quarterly UX OKRs, and manage their execution.
* Represent the company at conferences, in media, and in other public venues.
* Hold regular skip-level 1:1s with all members of their team.
* Hire a world-class team of Product Designers, UX Researchers, and Technical Writers and their managers.

#### Requirements

* A minimum of 10 years experience managing designers, researchers, and writers and leading design for a product company.
* Solid visual awareness with understanding of basic design principles like typography, layout, composition, and color theory.
* Passion for creating visually pleasing and intuitive user experiences.
* Collaborative team spirit with great communication skills.
* You share our [values](/handbook/values), and work in accordance with those values.
- [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#director-group)

#### Interview Process

* [Screening call](/handbook/hiring/#screening-call) with a recruiter.
* Interview with UX Manager. In this interview, the interviewer will spend a lot of time trying to understand the experience you have leading managers, as well as what type of teams you have led and your management style. The interviewer will also be looking to understand how you define strategy, how you've handled conflict, and how you dealt with difficult situations in the past. Do be prepared to talk about your work, experience with Design Systems, and technical ability, too.
* Interview with UX Director. In this interview, we will be looking for you to give some real insight into a problem you were solving as part of a large initiative you led the work on. We'll look to understand the size and structure of the team, the goals of the project, how you/the team approached research, how you used research to inform decisions, and how you collaborated with the wider team. Broadly, we want to hear how you identified what needed to be done and then guided your team to the end result.
* Interview with VP of Product.
* Interview with VP of Engineering.

As always, interviews and the screening call will be conducted via video.

See more details about our hiring process on the [hiring handbook](/handbook/hiring).

## Performance indicators

* [Hiring Actual vs Plan](/handbook/engineering/ux/performance-indicators/#hiring-actual-vs-plan)
* [Diversity](/handbook/engineering/ux/performance-indicators/#diversity)
* [Handbook Update Frequency](/handbook/engineering/ux/performance-indicators/#handbook-update-frequency)
* [Team Member Retention](/handbook/engineering/ux/performance-indicators/#team-member-retention)
